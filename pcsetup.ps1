﻿Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco install vscode -y
choco install notepadplusplus -y
choco install beyondcompare -y
choco install linqpad5 -y
choco install visualstudio2017enterprise -y
choco install visualstudio2017-workload-netweb -y
choco install visualstudio2017-workload-netcoretools -y
choco install git.install -y --params "/GitAndUnixToolsOnPath"
choco install dashlane -y
choco install firefox -y
choco install 7zip -y
choco install sysinternals -y
choco install curl -y
choco install paint.net -y
choco install nuget.commandline -y
choco install procexp -y
choco install dropbox -y
choco install awscli -y
choco install greenshot -y
choco install fiddler -y
choco install dotnetcore-sdk -y
choco install postman -y
choco install docker-for-windows -y
choco install vscode-docker -y
choco install awstools.powershell -y
choco install docker-kitematic -y